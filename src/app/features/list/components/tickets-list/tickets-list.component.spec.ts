import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TicketsListComponent } from './tickets-list.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { selectTickets, selectLoadingTickets, selectCreatingTicket, selectOnlyUncompleted } from '../../../../state/selectors';
import { RouterTestingModule } from '@angular/router/testing';

describe('TicketsListComponent', () => {
  let component: TicketsListComponent;
  let fixture: ComponentFixture<TicketsListComponent>;
  let store: MockStore;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TicketsListComponent],
      imports: [RouterTestingModule],
      providers: [provideMockStore({
        selectors: [
          {selector: selectTickets, value: [{description: 'test_ticket', id: 0, completed: true}]},
          {selector: selectLoadingTickets, value: false},
          {selector: selectCreatingTicket, value: false},
          {selector: selectOnlyUncompleted, value: false},
        ], initialState: {}
      })]
    }).compileComponents();

    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title in a h1 tag', (() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Tickets');
  }));

  it('should render test ticket', (() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.description').textContent).toContain('test_ticket');
  }));

  it('should hide completed ticket', (() => {
    store.overrideSelector(selectOnlyUncompleted, true);
    store.refreshState();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.description').textContent).not.toContain('test_ticket');
  }));
});
