import { Component, ChangeDetectionStrategy, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { selectTickets, selectCreatingTicket, selectLoadingTickets, selectOnlyUncompleted } from '../../../../state/selectors';
import { RouterModule } from '@angular/router';
import { ListPageActions } from '../../../../state/actions';

@Component({
  selector: 'app-tickets-list',
  templateUrl: './tickets-list.component.html',
  styleUrls: ['./tickets-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TicketsListComponent {
  public readonly tickets$ = this.store.select(selectTickets);
  public readonly creatingTicket$ = this.store.select(selectCreatingTicket);
  public readonly loadingTickets$ = this.store.select(selectLoadingTickets);
  public readonly onlyUncompleted$ = this.store.select(selectOnlyUncompleted);

  constructor(
    private readonly store: Store,
  ) {
    this.store.dispatch(ListPageActions.loadTicketsList());
  }

  addTicket(description: string) {
    this.store.dispatch(ListPageActions.addTicket({description}));
  }

  toggleFilter() {
    this.store.dispatch(ListPageActions.toggleFilter());
  }
}

@NgModule({
  declarations: [TicketsListComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [TicketsListComponent]
})
export class TicketsListComponentModule {
}
