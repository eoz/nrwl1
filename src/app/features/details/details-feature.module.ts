import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TicketComponent } from './components/ticket/ticket.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {path: ':id', component: TicketComponent}
    ]),
  ]
})
export class DetailsFeatureModule {

}