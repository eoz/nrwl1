import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TicketComponent } from './ticket.component';
import { TicketStore } from './ticket.store';
import { BackendService } from '../../../../backend.service';
import { ReplaySubject, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

class ActivatedRouteMock {
  params = new ReplaySubject<object>(1);
}

describe('TicketComponent', () => {
  let component: TicketComponent;
  let fixture: ComponentFixture<TicketComponent>;
  let route = new ActivatedRouteMock();
  let backendSrv = new BackendService();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TicketComponent],
      imports: [RouterTestingModule],
      providers: [
        TicketStore,
        {provide: BackendService, useValue: backendSrv},
        {provide: ActivatedRoute, useValue: route}
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load ticket', () => {
    spyOn(backendSrv, 'getTicket').and.returnValue(of({description: 'test_ticket'}));
    route.params.next({id: 0});
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('.description').textContent).toContain('test_ticket');
  });
});
