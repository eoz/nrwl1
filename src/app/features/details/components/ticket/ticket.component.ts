import { Component, ChangeDetectionStrategy, NgModule } from '@angular/core';
import { TicketStore } from './ticket.store';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TicketStore],
})
export class TicketComponent {
  public readonly state$ = this.store.state$;

  constructor(
    private readonly store: TicketStore,
  ) {
  }

  updateDescription(value: string) {
    this.store.updateTicketDescription$(value);
  }

  toggleEdit() {
    this.store.toggleEditMode$();
  }

  toggleCompleteness() {
    this.store.toggleTicketCompleteness$();
  }

  assignUser(value: string) {
    const id = parseInt(value, 10);
    if (id) {
      this.store.assignTicket$(id);
    }
  }

  loadUsers() {
    this.store.loadUsersList$();
  }
}

@NgModule({
  declarations: [TicketComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [TicketComponent]
})
export class TicketComponentModule {
}
