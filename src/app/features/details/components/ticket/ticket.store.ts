import { Injectable } from '@angular/core';
import { User } from '../../../../models/user.model';
import { Ticket } from '../../../../models/ticket.model';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { BackendService } from '../../../../backend.service';
import { ActivatedRoute } from '@angular/router';
import { takeUntil, switchMap, tap, finalize, filter, exhaustMap } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { concatLatestFrom } from '@ngrx/effects';

export interface TicketState {
  user?: User | undefined;
  ticket?: Partial<Ticket> | undefined;
  editMode?: boolean | undefined;
  loadingUser?: boolean | undefined;
  loadingTicket?: boolean | undefined;
  updatingTicket?: boolean | undefined;
  users?: User[] | undefined;
  loadingUsers?: boolean | undefined;
}

@Injectable()
export class TicketStore extends ComponentStore<TicketState> {
  constructor(
    private readonly backendSrv: BackendService,
    private readonly route: ActivatedRoute,
  ) {
    super({});
    this.route.params.pipe(takeUntil(this.destroy$)).subscribe(
      (params) => {
        if (params && params.hasOwnProperty('id')) {
          const id = parseInt(params.id, 10);
          if (id != null && typeof id === 'number') {
            this.loadTicket$(id);
          }
        }
      }
    );
  }

  readonly toggleEditMode$ = this.effect(_ => _.pipe(
    concatLatestFrom(() => this.select(s => s.editMode)),
    tap(([_, prev]) => this.patchState({editMode: !prev}))
  ));

  readonly assignTicket$ = this.effect<number>(_ => _.pipe(
    concatLatestFrom(() => this.select(s => s.ticket)),
    filter(([userId, ticket]) => !!userId && ticket?.id != null),
    tap(() => this.patchState({updatingTicket: true})),
    exhaustMap(([userId, ticket]) => this.backendSrv.assignTicket(ticket.id, userId).pipe(
      finalize(() => this.patchState({updatingTicket: false})),
      tapResponse(
        (ticket) => this.patchState({ticket}),
        (err) => {
          console.error(err);
          return EMPTY;
        }
      )
    ))
  ));

  readonly updateTicketDescription$ = this.effect<string>(_ => _.pipe(
    concatLatestFrom(() => this.select(s => s.ticket)),
    filter(([_, ticket]) => ticket?.id != null),
    tap(() => this.patchState({updatingTicket: true})),
    exhaustMap(([description, ticket]) => this.backendSrv.updateTicket(ticket.id, {description}).pipe(
      finalize(() => this.patchState({updatingTicket: false})),
      tapResponse(
        (ticket) => this.patchState({ticket}),
        (err) => {
          console.error(err);
          return EMPTY;
        }
      )
    ))
  ));

  readonly toggleTicketCompleteness$ = this.effect(_ => _.pipe(
    concatLatestFrom(() => this.select(s => s.ticket)),
    filter(([_, ticket]) => ticket?.id != null),
    tap(() => this.patchState({updatingTicket: true})),
    exhaustMap(([_, ticket]) => this.backendSrv.completeTicket(ticket.id, !ticket.completed).pipe(
      finalize(() => this.patchState({updatingTicket: false})),
      tapResponse(
        (ticket) => this.patchState({ticket}),
        (err) => {
          console.error(err);
          return EMPTY;
        }
      )
    ))
  ));

  readonly loadUsersList$ = this.effect(_ => _.pipe(
    concatLatestFrom(() => this.select(s => s.users)),
    filter(([_, users]) => !users?.length),
    tap(() => this.patchState({loadingUsers: true})),
    switchMap(() => this.backendSrv.getUsers().pipe(
      finalize(() => this.patchState({loadingUsers: false})),
      tapResponse(
        (users) => this.patchState({users}),
        (err) => {
          console.error(err);
          return EMPTY;
        }
      )
    ))
  ));

  private readonly loadTicket$ = this.effect<number>(_ => _.pipe(
    tap(() => this.patchState({loadingTicket: true})),
    switchMap((id) => this.backendSrv.getTicket(id).pipe(
      finalize(() => this.patchState({loadingTicket: false})),
      tapResponse(
        (ticket) => {
          this.patchState({ticket});
          if (ticket.assigneeId) {
            this.loadUser$(ticket.assigneeId);
          }
        },
        (err) => {
          console.error(err);
          return EMPTY;
        }
      ))
    ))
  );

  private readonly loadUser$ = this.effect<number>(_ => _.pipe(
    tap(() => this.patchState({loadingUser: true})),
    switchMap((id) => this.backendSrv.getUser(id).pipe(
      finalize(() => this.patchState({loadingUser: false})),
      tapResponse(
        (user) => this.patchState({user}),
        (err) => {
          console.error(err);
          return EMPTY;
        }
      ))
    ))
  );
}
