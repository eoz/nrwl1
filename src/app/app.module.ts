import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BackendService } from './backend.service';
import { TicketsListComponent } from './features/list/components/tickets-list/tickets-list.component';
import { RouterModule } from '@angular/router';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppReducers } from './state/reducers';
import { AppEffects } from './state/effects';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: '', component: TicketsListComponent},
      {path: 'ticket', loadChildren: () => import('./features/details/details-feature.module').then(m => m.DetailsFeatureModule)}
    ]),
    StoreDevtoolsModule.instrument({
        maxAge: 25,
        logOnly: false, // environment.production,
      }
    ),
    StoreModule.forRoot(AppReducers, {
      runtimeChecks: {
        strictStateSerializability: true,
        strictActionSerializability: true,
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictActionWithinNgZone: true,
        strictActionTypeUniqueness: true
      }
    }),
    EffectsModule.forRoot([AppEffects]),
  ],
  providers: [BackendService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }

}
