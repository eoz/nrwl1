import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BackendService } from '../backend.service';
import { ApiActions, ListPageActions } from './actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';

@Injectable()
export class AppEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly backendSrv: BackendService, // no facades ftw!
  ) {}

  /**
   * Convert page action into API request.
   */
  private readonly loadTicketsRequest$ = createEffect(() => this.actions$.pipe(
    ofType(ListPageActions.loadTicketsList),
    map(() => ApiActions.ticketsLoadRequest())
  ));

  private readonly loadTicketsStart$ = createEffect(() => this.actions$.pipe(
    ofType(ApiActions.ticketsLoadRequest),
    // Here we can decide if we should start loading.
    map(() => ApiActions.ticketsLoadStart())
  ));

  private readonly loadTickets$ = createEffect(() => this.actions$.pipe(
    ofType(ApiActions.ticketsLoadStart),
    switchMap(() => this.backendSrv.getTickets().pipe(
      map((tickets) => ApiActions.ticketsLoadSuccess({tickets})),
      catchError((err: HttpErrorResponse) => of(ApiActions.ticketsLoadFail({error: err.message})))
    ))
  ));

  private readonly createTicketRequest$ = createEffect(() => this.actions$.pipe(
    ofType(ListPageActions.addTicket),
    map((action) => ApiActions.createTicketRequest({description: action.description}))
  ));

  private readonly createTicketStart$ = createEffect(() => this.actions$.pipe(
    ofType(ApiActions.createTicketRequest),
    map((action) => ApiActions.createTicketStart({description: action.description}))
  ));

  private readonly createTicket$ = createEffect(() => this.actions$.pipe(
    ofType(ApiActions.createTicketStart),
    switchMap((action) => this.backendSrv.createTicket({description: action.description}).pipe(
      map((ticket) => ApiActions.createTicketSuccess({id: ticket.id})),
      catchError((err: HttpErrorResponse) => of(ApiActions.createTicketFail({error: err.message})))
    ))
  ));

  private readonly reloadWhenTicketCreated$ = createEffect(() => this.actions$.pipe(
    ofType(ApiActions.createTicketSuccess),
    map(() => ApiActions.ticketsLoadRequest())
  ));
}