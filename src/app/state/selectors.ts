import { AppState } from './reducers';
import { createSelector, createFeatureSelector } from '@ngrx/store';

const featureSelector = createFeatureSelector<AppState>('app');

export const selectTickets = createSelector(featureSelector, (state) => state.tickets);

export const selectCreatingTicket = createSelector(featureSelector, (state: AppState) => state.creatingTicket);

export const selectLoadingTickets = createSelector(featureSelector, (state: AppState) => state.loadingTickets);

export const selectOnlyUncompleted = createSelector(featureSelector, (state: AppState) => state.onlyUncompleted);
