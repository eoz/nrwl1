import { createAction, props } from '@ngrx/store';

export const loadTicketsList = createAction(
  '[List Page] Load List'
);

export const addTicket = createAction(
  '[List Page] Add Ticket',
  props<{ description: string }>()
);

export const toggleFilter = createAction(
  '[List Page] Toggle Filter'
);