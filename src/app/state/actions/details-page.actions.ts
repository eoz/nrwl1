import { createAction, props } from '@ngrx/store';

export const loadTicket = createAction(
  '[Details Page] Load Ticket',
  props<{ id: number }>()
);