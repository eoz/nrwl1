import { createAction, props } from '@ngrx/store';
import { Ticket } from '../../models/ticket.model';

export const ticketsLoadRequest = createAction(
  '[API] Load Tickets Request'
);

export const ticketsLoadStart = createAction(
  '[API] Load Tickets Start'
);

export const ticketsLoadFail = createAction(
  '[API] Load Tickets Fail',
  props<{ error: string }>(),
);

export const ticketsLoadSuccess = createAction(
  '[API] Load Tickets Success',
  props<{ tickets: Ticket[] }>()
);

export const createTicketRequest = createAction(
  '[API] Create Ticket Request',
  props<{ description: string }>()
);

export const createTicketStart = createAction(
  '[API] Create Ticket Start',
  props<{ description: string }>()
);

export const createTicketFail = createAction(
  '[API] Create Ticket Fail',
  props<{ error: string }>(),
);

export const createTicketSuccess = createAction(
  '[API] Create Ticket Success',
  props<{ id: number }>()
);

