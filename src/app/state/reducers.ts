import { createReducer, on } from '@ngrx/store';
import { Ticket } from '../models/ticket.model';
import { ApiActions, ListPageActions } from './actions';

export interface AppState {
  tickets?: Ticket[] | undefined;
  loadingTickets?: boolean | undefined;
  ticketsLoadingError?: string | undefined;
  creatingTicket?: boolean | undefined;
  ticketCreationError?: string | undefined;
  onlyUncompleted?: boolean | undefined;
}

const initialState: AppState = {
  tickets: undefined,
};

export const reducer = createReducer<AppState>(initialState,
  on(ApiActions.ticketsLoadStart, (state) => ({
    ...state,
    loadingTickets: true,
    tickets: undefined,
    ticketsLoadingError: undefined,
  })),
  on(ApiActions.ticketsLoadFail, (state, action) => ({
    ...state,
    loadingTickets: false,
    ticketsLoadingError: action.error,
  })),
  on(ApiActions.ticketsLoadSuccess, (state, action) => ({
    ...state,
    tickets: action.tickets,
    loadingTickets: false,
    ticketsLoadingError: undefined,
  })),
  on(ApiActions.createTicketStart, (state) => ({
    ...state,
    creatingTicket: true,
    ticketCreationError: undefined,
  })),
  on(ApiActions.createTicketSuccess, (state) => ({
    ...state,
    creatingTicket: false,
  })),
  on(ApiActions.createTicketFail, (state, action) => ({
    ...state,
    creatingTicket: false,
    ticketCreationError: action.error,
  })),
  on(ListPageActions.toggleFilter, (state) => ({
    ...state,
    onlyUncompleted: !state.onlyUncompleted,
  }))
);

export const AppReducers = {app: reducer};